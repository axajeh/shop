db.createCollection("users", {
  validator: {
    $jsonSchema: {
      bsonType: "object",
      required: ["username", "password", "role", "isActive", "isDeleted", "userReg", "createdAt", "updatedAt"],
      properties: {
        username: {
          bsonType: 'string'
        },
        firstname: {
          bsonType: 'string'
        },
        lastname: {
          bsonType: 'string'
        },
        email: {
          bsonType: 'string'
        },
        phoneNumber: {
          bsonType: 'int'
        },
        password: {
          bsonType: 'string'
        },
        imageUrl: {
          bsonType: 'string'
        },
        address: {
          bsonType: 'object',
          required: ["state", "city"],
          properties: {
            state: {
              bsonType: 'int',
            },
            city: {
              bsonType: 'int',
            },
            add: {
              bsonType: 'string'
            },
            postcode: {
              bsonType: 'int',
            },
          }
        },
        role: {
          bsonType: 'string'
        },
        isDeleted: {
          bsonType: "bool"
        },
        isActive: {
          bsonType: "bool"
        },
        userReg: {
          bsonType: "string"
        },
        createdAt: {
          bsonType: "string"
        },
        updatedAt: {
          bsonType: "string"
        }
      }
    }
  }
})