declare global {
  namespace Schema {
    interface User {
      _id: string,
      username: string,
      firstname: string,
      lastname: string,
      email: string,
      password: string,
      phoneNumber: number,
      imageUrl: string,
      address: Array<{ state: number, city: number, add: string, postcode: number }>,
      role: string,
      isDeleted: boolean,
      isActive: boolean,
      userReg: string,
      createdAt: string,
      updatedAt: string
    }
  }
}

export { }