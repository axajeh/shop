import mongodb, { Db, ObjectID } from 'mongodb'

class Database {
  private _db: Db
  private _mongodbUrl = process.env.DATA_BASE_URL
  public types = {
    ObjectID
  }

  public get db() {
    if (this._db) return this._db
    return null
  }

  public async connect() {
    try {
      const client = await mongodb.MongoClient.connect(this._mongodbUrl, { useUnifiedTopology: true })

      this._db = client.db()
      return client
    } catch (error) {
      console.log(error)
      return null
    }

  }
}

export default new Database()