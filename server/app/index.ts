import express, { Application } from 'express'

import Database from './service/Database'
import Routes from './routes/index'

class App {

  app: Application = express()

  constructor() {
    this.setupRoutes()
    this.setupServer()
  }


  setupRoutes() {
    this.app.use('/api', Routes)
  }


  async setupServer() {
    const client = await Database.connect()
    if (client) {
      this.app.listen(process.env.SERVER_PORT, () => {
        console.log(`connected loclhost:${process.env.SERVER_PORT}`)
      })
    }
  }
}

export default App