import autoBind from 'auto-bind'

import Database from '../service/Database'

class Model {
  public db = Database.db
  protected types = Database.types


  constructor() {
    autoBind(this)
  }
}

export default Model